FROM ruby:3.1.2-alpine

# Установка временной зоны
RUN apk add --update tzdata \
    && cp /usr/share/zoneinfo/Europe/Moscow /etc/localtime \
    && echo "Europe/Moscow" > /etc/timezone

# Установка runtime-зависимостей
RUN apk --no-cache add --update --virtual runtime-deps \
    postgresql-client \
    nodejs \
    libffi-dev \
    readline

# Установка зависимостей для компиляции RubyGems и выполнение установки гемов
WORKDIR /tmp
ADD Gemfile* ./
RUN apk --no-cache add --virtual build-deps \
        build-base \
        openssl-dev \
        postgresql-dev \
        libc-dev \
        linux-headers \
        libxml2-dev \
        libxslt-dev \
        readline-dev \
    && bundle install --jobs=2 \
    && apk del build-deps


# Копирование приложения в контейнер
ENV APP_ROOT /app_root
COPY . $APP_ROOT
WORKDIR $APP_ROOT

# Открытие порта 3000
EXPOSE 3000

